#!/bin/bash

contractVersion=$(pact-broker describe-version \
  --pacticipant $SERVICE_NAME \
  --latest docker:$SERVICE_VERSION \
  --output=json \
  | jq -r '.number')

# Fail in case contract version is not found

echo $contractVersion

canIDeploy=$(pact-broker can-i-deploy \
  --pacticipant $SERVICE_NAME \
  --version $contractVersion \
  --to prod \
  --output=json\
  | jq -r '.summary.deployable')

echo $canIDeploy

if [ $canIDeploy ]; then
    echo "Deploying"
    helm version
    #helm3 upgrade --install \
#		-f namespace/${ENV}.yaml \
#		-f values.yaml \
#		--set env=${ENV} \
#		--set affinityTopologyKey=${AFFINITY_TOPOLOGY_KEY} \
#		--namespace ${NAMESPACE} \
#		payworks-service-${SERVICE_NAME} .
else
  echo "Cannot deploy"
  return
fi
