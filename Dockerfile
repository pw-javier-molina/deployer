FROM ruby:2.6-alpine3.12

ENV PACT_BROKER_BASE_URL=https://pactbroker-eu-w1.dev.payworks.io/

RUN gem install pact_broker-client

RUN apk update
RUN apk add bash
RUN apk add jq

RUN wget https://get.helm.sh/helm-v3.4.2-linux-amd64.tar.gz \
    && tar -zxvf helm-v3.4.2-linux-amd64.tar.gz \
    && mv linux-amd64/helm /bin/helm

COPY deploy.sh deploy.sh

CMD ["bash", "deploy.sh"]